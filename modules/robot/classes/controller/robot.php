<?php
class Controller_Robot extends Controller_Common {
        /**
         * !!!
         */
         public function action_getParams(){
                $this->auto_render = false;
                
                // set params
                if(strlen($this->request->query('param')) > 0){
                    $params = array($this->request->query('param'));
                    $m = new Model_Robot();
                    $result = $m->getSearchParams($params);
                    
                    if(strlen($this->request->query('request')) > 0){
                        foreach($result[$this->request->query('param')] as $rKey => $rItem){
                            if(!preg_match("|". mb_convert_case($this->request->query('request'), MB_CASE_LOWER, "UTF-8") ."|", mb_convert_case($rItem, MB_CASE_LOWER, "UTF-8"))){
                                unset($result[$this->request->query('param')][$rKey]);
                            }
                        }
                    }
                } else {
                    $result = array('error' => '1', 'message' => 'Error. Bad request.');
                }
                $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode($result));
         }

}// End Currency Controller
?>
