<?php
class Model_Robot extends ORM {

        /*
         * предопределения
         */
        protected $_primary_key = 'id';
        protected $_table_name = 'robots';

	/**
	 * Получение параметров терминалов для составления критериев поиска
         *  - Город
         *  - Место
	 * @param array $params Список параметров выборки
	 * @return string
	 */
	public function getSearchParams($params)
	{
            $exParams = array('city', 'place');
            $result = array();
            
            foreach($params as $param){
                if(!in_array($param, $exParams)){
                    continue;
                }

                $obResult = $this->select(array($param, $param))
                        ->group_by($param)
                        ->order_by($param, 'asc')
                        ->find_all();
                foreach( $obResult as $key => $value ){
                    $result[$param][] = $value->{$param};
                }
            }
            
            return $result;
	}
        
	/**
	 * Добавление робота
	 * @param array $params Список параметров робота
	 * @return none
	 */
	public function addRobot($params)
	{
            $this->create();
            $this->city = $params['city'];
            $this->address = $params['address'];
            $this->place = $params['place'];
            $this->status = (int)($params['init']=='on');
            $this->save();
	}

	/**
	 * Обновляет время последнего запроса
	 * @param none
	 * @return none
	 */
	public function updateStatus($id)
	{
            $item = ORM::factory('robot', $id);
            $item->last_update = time();
            $item->save();
	}
        
	/**
	 * Обновляет количество загруженных фото
	 * @param none
	 * @return none
	 */
	public function addPhotoCount($id)
	{
            $item = ORM::factory('robot', $id);
            $item->count_photos = (int)$item->count_photos + 1;
            $item->save();
	}

    public function getRobotPlace($id){
        $item = ORM::factory('robot', $id);

        return array('city' => $item->city, 'place' => $item->place, 'id' => $item->id);
    }
} // End Currency Model
?>