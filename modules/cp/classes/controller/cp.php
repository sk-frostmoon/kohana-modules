<?php
class Controller_Cp extends Controller_Common {

    /*
     * Авторизация в админке
     */
    public function action_login(){
        $this->template->set_filename('main/cp_login');
        $this->template->title .= ' | ' . __('cp_main_title');
        $this->auto_render = true;
        
        $error = array();

        if(Auth::instance()->logged_in() && Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/main/');
        }

        if(HTTP_Request::POST == $this->request->method()) {
            // Attempt to login user
            $remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
            $user = Auth::instance()->login($this->request->post('login'), $this->request->post('password'), $remember);
            
            // If successful, redirect user
            if($user) {
                Request::current()->redirect('/cp/main/');
            } else {
                $error = array('login' => __('cp_fail_login'));
            }
        }
        
        $form = View::factory('cp/login')
                ->set('auth', Auth::instance())
                ->set('error', $error);
        $this->response->body($form);
        $this->template->content = $form;
    }
    
    /*
     * Логаут в админке
     */
    public function action_logout() 
    {
        // Log user out
        Auth::instance()->logout();
         
        // Redirect to login page
        Request::current()->redirect('/cp/login/');
    }
    
    /*
     * Главная админки
     */
    public function action_main(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/login/');
        }
        $model = new Model_Cp();
        $model->getAdminMenu();

        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . __('cp_main_title');
        $this->auto_render = true;

        $form = View::factory('cp/main')
                ->set('auth', Auth::instance());
        $this->response->body($form);
        $this->template->content = $form;
    }
    
    /*
     * Список пользователей
     */
    public function action_usersList(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/main/');
        }
        $model = new Model_Cp();
        $model->getAdminMenu();

        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . __('cp_users');
        $this->auto_render = true;

        $users = ORM::factory('user')->find_all()->as_array('id');
        
        $form = View::factory('cp/usersList')
                ->set('auth', Auth::instance())
                ->set('users', $users);
        $this->response->body($form);
        $this->template->content = $form;
    }
    
    /*
     * Список пользователей
     */
    public function action_mediaList(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/main/');
        }

        $model = new Model_Cp();
        $model->getAdminMenu();

        $model = new Model_Media();
        $media = $model->getAllMediaForModerate();

        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . __('cp_moderating');
        $this->auto_render = true;

       
        $form = View::factory('cp/mediaList')
                ->set('auth', Auth::instance())
                ->set('media', $media);
        $this->response->body($form);
        $this->template->content = $form;
    }
    
    /*
     * Список роботов
     */
    public function action_robotsList(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/main/');
        }
        $model = new Model_Cp();
        $model->getAdminMenu();

        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . __('cp_robots');
        $this->auto_render = true;

        $robots = ORM::factory('robot')->find_all()->as_array('id');
        
        $form = View::factory('cp/robotsList')
                ->set('auth', Auth::instance())
                ->set('robots', $robots);
        $this->response->body($form);
        $this->template->content = $form;
    }
    
    /*
     * Список роботов
     */
    public function action_robotsAdd(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/main/');
        }
        $model = new Model_Cp();
        $model->getAdminMenu();
        
        if(HTTP_Request::POST == $this->request->method()){
            $error = false;
            $post = Arr::extract($_POST, array('city', 'address', 'place', 'init'));
            
            if(!empty($post['city']) && !empty($post['address']) && !empty($post['place'])){
                // save to db
                $model_robot = new Model_Robot();
                $model_robot->addRobot($post);
            } else {
                $error = true;
            }
        }
        
        if(isset($error) && !$error){
            Request::current()->redirect('/cp/robotsList/');
        }

        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . __('cp_robot_add');
        $this->auto_render = true;

        $form = View::factory('cp/robotsAdd')
                ->set('auth', Auth::instance())
                ->set('error', (isset($error) && $error));
        $this->response->body($form);
        $this->template->content = $form;            
    }
    
    /*
     * Список роботов
     */
    public function action_robotsEdit(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/main/');
        }
        if(!$this->request->param('id') || !((int)$this->request->param('id') > 0)){
            Request::current()->redirect('/cp/robotsList/');
        }
        
        $model = new Model_Cp();
        $model->getAdminMenu();
        
        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . __('cp_edit');
        $this->auto_render = true;

        $robot = new Model_Robot((int)$this->request->param('id'));
        $template = new Model_Rctempl();
        $templates = array(
            'color' => $template->getColors(),
            'sound' => $template->getSounds(),
            'iso'   => $template->getIsos()
        );

        $templates['iso'] = json_decode($templates['iso']->value);
        $isos = array(
            '0' => 'auto',
            '1' => '100',
            '2' => '200',
            '3' => '400',
            '4' => '800',
            '5' => '1600',
            '6' => '3200',
            '7' => '6400',
        );
        foreach($isos as $k => $v){
            if(!in_array($k, $templates['iso'])){
                unset($isos[$k]);
            }
        }
        $templates['iso'] = $isos;

        $form = View::factory('cp/robotsEdit')
                ->set('auth', Auth::instance())
                ->set('error', (isset($error) && $error))
                ->set('robot', $robot)
                ->set('templates', $templates);
        $this->response->body($form);
        $this->template->content = $form;
    }

    /**
     * Админский раздел предопределения настроек роботов
     */
    public function action_configList(){
        if(!Auth::instance()->logged_in() || !Auth::instance()->logged_in('admin')){
            Request::current()->redirect('/cp/login/');
        }
        $model = new Model_Cp();
        $model->getAdminMenu();

        $this->template->set_filename('main/cp_main');
        $this->template->title .= ' | ' . "Панель настроек";
        $this->auto_render = true;

        if($this->request->param('id') && strlen($this->request->param('id')) > 0){
            switch($this->request->param('id')){
                case "color":
                    $form = $this->configListColor($model);
                    break;
                case "sound":
                    $form = $this->configListSound($model);
                    break;
                case "iso":
                    $form = $this->configListIso($model);
                    break;
            }

        } else {
            $form = View::factory('cp/configList')->set('auth', Auth::instance());
        }

        $this->response->body($form);
        $this->template->content = $form;
    }

    /**
     * предопределение цвета
     * @param $model
     * @return Kohana_View
     */
    public function configListColor($model){
        $form = View::factory('cp/configListColor')->set('auth', Auth::instance());
        $error = array('error' => '0', 'msg' => '');
        $model = new Model_Rctempl();
        if(HTTP_Request::POST == $this->request->method()){
            $post = Arr::extract($_POST, array('action'));
            switch($post['action']){
                case "add":
                    $post = array_merge($post, Arr::extract($_POST, array('name', 'value')));
                    if(strlen($post['name']) > 0 && strlen($post['value'])> 0){
                        $model->addColor($post);
                    } else {
                        $error['error'] = '1';
                        $error['msg'] = 'Пустые значения';
                    }
                    break;
                case "delete":
                    $post = array_merge($post, Arr::extract($_POST, array('id')));
                    if(strlen($post['id']) > 0){
                        $model->delColor($post);
                    } else {
                        $error['error'] = '1';
                        $error['msg'] = 'Пустой ID';
                    }
                    break;
            }

            if($error['error'] == '0'){
                Request::current()->redirect('/cp/configList/color/');
            }
        }

        $form->set('error', $error);
        $form->set('options', $model->getColors());
        return $form;
    }

    /**
     * Предопределения звуков
     * @param $model
     * @return Kohana_View
     */
    public function configListSound($model){
        $form = View::factory('cp/configListSound')->set('auth', Auth::instance());
        $error = array('error' => '0', 'msg' => '');
        $model = new Model_Rctempl();
        if(HTTP_Request::POST == $this->request->method()){
            $post = Arr::extract($_POST, array('action'));
            switch($post['action']){
                case "add":
                    $post = array_merge($post, Arr::extract($_POST, array('name', 'value')));
                    if(strlen($post['name']) > 0 && !empty($_FILES)){
                        $model->addSound($post, $_FILES);
                    } else {
                        $error['error'] = '1';
                        $error['msg'] = 'Пустые значения';
                    }
                    break;
                case "delete":
                    $post = array_merge($post, Arr::extract($_POST, array('id')));
                    if(strlen($post['id']) > 0){
                        $model->delSound($post);
                    } else {
                        $error['error'] = '1';
                        $error['msg'] = 'Пустой ID';
                    }
                    break;
            }

            if($error['error'] == '0'){
                Request::current()->redirect('/cp/configList/sound/');
            }
        }

        $form->set('error', $error);
        $form->set('options', $model->getSounds());
        return $form;
    }

    /**
     * Предопределения ISO
     * @param $model
     * @return Kohana_View
     */
    public function configListIso($model){
        $form = View::factory('cp/configListIso')->set('auth', Auth::instance());
        $model = new Model_Rctempl();
        $error = array('error' => '0', 'msg' => '');
        if(HTTP_Request::POST == $this->request->method()){
            $post = Arr::extract($_POST, array('iso'));
            $isos = explode(",", $post['iso']);
            foreach($isos as $k => $i){
                $i = trim($i);
                if($i >= 0 && $i <= 7){
                    $isos[$k] = $i;
                } else {
                    $error['error'] = '1';
                    $error['msg'] = 'Out of range';
                }
            }
            if($error['error'] == '0'){
                sort($isos);
                $template = $model->getIsos();
                $template->value = json_encode($isos);
                $template->save();

                Request::current()->redirect('/cp/configList/iso/');
            }
        }

        $form->set('error', $error);
        $form->set('options', $model->getIsos());
        return $form;
    }
}
?>