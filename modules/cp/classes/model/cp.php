<?php
class Model_Cp extends ORM {
    protected $_primary_key = 'id';
    protected $_table_name = 'cp_log';

    /*
     * Меню в админке
     */
    public function getAdminMenu(){
        // top menu
        $topMenu = array('main' => array('title' => __('cp_main_page'), 'url' => 'cp/main'));
        View::set_global('topMenu',$topMenu);

        // left menu
        $leftMenu = array(
            'main' => array('title' => __('cp_main_page'), 'url' => 'cp/main', 'alternate' => array()),
            'users' => array('title' => __('cp_users'), 'url' => 'cp/usersList', 'alternate' => array()),
            'moderating' => array('title' => __('cp_moderating'), 'url' => 'cp/mediaList', 'alternate' => array()),
            'robots' => array('title' => __('cp_robots'), 'url' => 'cp/robotsList', 'alternate' => array('cp/robotsAdd')),
            'config' => array('title' => "Панель настроек", 'url' => 'cp/configList', 'alternate' => array()),
            );
        View::set_global('leftMenu', $leftMenu);
    }
} // End Currency Model
?>
