<table class="table table-hover table-bordered">
    <tr>
        <td colspan="7" style="text-align: right;">
            <button class="btn btn-primary" onclick="window.location='/cp/robotsAdd/';"><?=__('cp_robot_add');?></button>
            <button class="btn btn-success">Включить</button>
            <button class="btn btn-danger">Выключить</button>
            <button class="btn btn-info">Применить шаблон</button>
            <button class="btn btn-info">Обновить конфигурацию</button>
        </td>
    </tr>
    <tr>
        <th>ID</th>
        <th><?=__('cp_robot_address');?></th>
        <th><?=__('cp_robot_photo');?></th>
        <th><?=__('cp_robot_video');?></th>
        <th><?=__('cp_robot_init');?></th>
        <th><?=__('cp_robot_last_update');?></th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
    <?
    foreach($robots as $robot){
        //посчитаем статус
        $addClass = '';
        if($robot->status == '1'){
            $curTime = time();
            if($curTime < $robot->last_update || ((time() - $robot->last_update) <= 5)){
                $addClass = 'warning';
            } else if((time() - $robot->last_update) > 10 && (time() - $robot->last_update) < 600) {
                $addClass = 'success';
            } else {
                $addClass = 'error';
            }
        } else {
            $addClass = 'info';
        }
        
        ?>
        <tr class="<?=$addClass;?>">
            <td><?=$robot->id;?></td>
            <td><?=$robot->city ." | ". $robot->address ." | ". $robot->place ;?></td>
            <td><?=(int)$robot->count_photos;?></td>
            <td><?=(int)$robot->count_videos;?></td>
            <td><?=$robot->status == '1'?'on':'off';?></td>
            <td><?=date('d.m.y H:i:s', $robot->last_update);?></td>
            <td style="text-align: center;"><a href="/cp/robotsEdit/<?=$robot->id;?>/" alt="<?=__('cp_edit');?>"><i class="icon-pencil"></i></a></td>
            <td><input type="checkbox" data-id="<?=$robot->id;?>"/></td>
        </tr>
        <?
    }
    ?>
</table>

