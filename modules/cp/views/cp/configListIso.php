<h3>Шаблоны ISO</h3>

<?
if($error['error'] == '1'){
    var_dump($error);
}
?>

<form action="" method="POST" enctype="multipart/form-data">
    <table class="table table-hover table-bordered configList">
        <tr>
            <td colspan="3"><center>Список возможных значений ISO</center></td>
        </tr>
        <tr>
            <td>
                <div class="input-prepend">
                    <input name="iso" type="text" value="<?=implode(", ", json_decode($options->value));?>">
                </div>
            </td>
            <td>
                <button class="btn btn-primary">Сохранить</button>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Справка</h3>
                0 => Автоматически <br />
                1 => 100 <br />
                2 => 200 <br />
                3 => 400 <br />
                4 => 800 <br />
                5 => 1600 <br />
                6 => 3200 <br />
                7 => 6400 <br />
            </td>
        </tr>
    </table>
</form>