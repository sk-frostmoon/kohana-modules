<style type="text/css">
    body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
    }

    .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
        -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
        box-shadow: 0 1px 2px rgba(0,0,0,.05);
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin input[type="text"],
    .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
    }

</style>
<div class="container">

    <form class="form-signin" method="POST">
        <h2 class="form-signin-heading"><?= __('cp_need_auth'); ?></h2>
        <div class="control-group <?= count($error) > 0 ? 'error' : ''; ?>">
            <label class="control-label" for="inputLogin"><?= __('user_username'); ?></label>
            <div class="controls">
                <input type="text" id="inputLogin" name="login" placeholder="<?= __('user_username'); ?>">
            </div>
        </div>
        <div class="control-group <?= count($error) > 0 ? 'error' : ''; ?>">
            <label class="control-label" for="inputPassword"><?= __('user_password'); ?></label>
            <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="<?= __('user_password'); ?>">
            </div>
        </div>
        <label class="checkbox">
            <input type="checkbox" value="remember"><?= __('user_remember'); ?>
        </label>
        <button class="btn btn-large btn-primary" type="submit"><?= __('user_logon'); ?></button>
    </form>

</div>