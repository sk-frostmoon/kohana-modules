<style>
    .moderateImage {
        width: 192px;
        height: 192px;
        float: left;
        margin: 3px;
        background-size: 192px 192px;
    }
    .moderateButtonBlock {
        height: 30px;
        width: 162px;
        margin-left: 15px;
        margin-top: 142px;
    }
</style>
    
<?
foreach($media as $mediaId => $mediaEl){
    ?><div class="moderateImage" style="background-image: url(<?=$mediaEl['src']['thumb'];?>);">
        <div class="moderateButtonBlock" data-id="<?=$mediaId;?>">
            <button class="btn btn-success" style="float: left;">+</button>
            <button class="btn btn-danger" style="float: right;">-</button>
        </div>
    </div><?
}
?>