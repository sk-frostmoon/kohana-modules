<div class="span3">&nbsp;</div>
<div class="span4">
    <form class="form-horizontal" method="POST">
        <? if($error){ ?>
        <div class="control-group" style="font-weight: bold;">
            <label class="control-label"><p class="text-error"><?=__('cp_robot_add_error');?></p></label>
            <div class="controls" style="padding-top: 5px;">
                <p class="text-error"><?=__('cp_robot_add_error_ex');?></p>
            </div>
        </div>
        <? } ?>
        <div class="control-group">
            <label class="control-label" for="robotCity"><?=__('cp_robot_add_city');?></label>
            <div class="controls">
                <input type="text" id="robotCity" name="city" placeholder="<?=__('cp_robot_add_city');?>">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="robotAddress"><?=__('cp_robot_add_address');?></label>
            <div class="controls">
                <input type="text" id="robotCity" name="address" placeholder="<?=__('cp_robot_add_address');?>">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="robotPlace"><?=__('cp_robot_add_place');?></label>
            <div class="controls">
                <input type="text" id="robotCity" name="place" placeholder="<?=__('cp_robot_add_place');?>">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="robotInit"><?=__('cp_robot_init');?></label>
            <div class="controls">
                <input type="checkbox" name="init" id="robotInit"> 
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary"><?=__('cp_robot_add');?></button>
            </div>
        </div>
    </form>
</div>