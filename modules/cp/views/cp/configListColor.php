<h3>Шаблоны цвета</h3>

<?
if($error['error'] == '1'){
    var_dump($error);
}
?>
<table class="table table-hover table-bordered configList">
    <? if(count($options) == 0){ ?>
    <tr>
        <td><center>Пусто</center></td>
    </tr>
    <? } else { ?>
        <tr>
            <th>Название</th>
            <th>Цвет</th>
            <th>Значение</th>
        </tr>
        <? foreach($options as $option){ ?>
            <tr>
                <td><?=$option->name;?></td>
                <td style="background-color: rgb(<?=$option->value;?>);">&nbsp;</td>
                <td><?=$option->value;?></td>
            </tr>
        <? } ?>
    <? } ?>
</table>

<form action="" method="POST">
    <table class="table table-hover table-bordered configList">
        <tr>
            <td colspan="3"><center>Добавить новый</center></td>
        </tr>
        <tr>
            <td>
                <input type="text" name="name" placeholder="Название">
            </td>
            <td>
                <div class="input-append color" data-color="rgb(255, 255, 255)" data-color-format="rgb">
                    <input type="text" name="value" value="rgb(255, 255, 255)" readonly="" id="robotPhotoLight">
                    <span class="add-on"><i style="background-color: rgb(255, 255, 255);"></i></span>
                </div>
            </td>
            <td>
                <input type="hidden" value="add" name="action" />
                <button class="btn btn-primary">Добавить</button>
            </td>
        </tr>
    </table>
</form>