<table class="table table-hover table-bordered">
    <tr>
        <th>ID</th>
        <th><?=__('user_username');?></th>
        <th><?=__('cp_user_role');?></th>
        <th><?=__('user_email');?></th>
        <th><?=__('cp_last_login');?></th>
        <th><?=__('cp_login_count');?></th>
        <th>&nbsp;</th>
    </tr>
    <?
    foreach($users as $user){
        ?>
        <tr>
            <td><?=$user->id;?></td>
            <td><?=$user->username;?></td>
            <td><?=$user->roles;?></td>
            <td><?=$user->email;?></td>
            <td><?=date('d.m.y H:i:s', $user->last_login);?></td>
            <td><?=$user->logins;?></td>
            <td style="text-align: center;"><a href="#" alt="<?=__('cp_edit');?>"><i class="icon-pencil"></i></a> &nbsp; <a href="#" alt="<?=__('cp_delete');?>"><i class="icon-remove"></i></a></td>
        </tr>
        <?
    }
    ?>
</table>

