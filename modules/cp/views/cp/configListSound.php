<h3>Шаблоны звука</h3>

<?
if($error['error'] == '1'){
    var_dump($error);
}
?>
<table class="table table-hover table-bordered configList">
    <? if(count($options) == 0){ ?>
    <tr>
        <td><center>Пусто</center></td>
    </tr>
    <? } else { ?>
        <tr>
            <th>Название</th>
            <th>Звук</th>
            <th>Значение</th>
        </tr>
        <? foreach($options as $option){ ?>
            <tr>
                <td><?=$option->name;?></td>
                <td><audio src="/upload/sound/<?=$option->value;?>" controls>KLOL</audio></td>
                <td><?=$option->value;?></td>
            </tr>
        <? } ?>
    <? } ?>
</table>

<form action="" method="POST" enctype="multipart/form-data">
    <table class="table table-hover table-bordered configList">
        <tr>
            <td colspan="3"><center>Добавить новый</center></td>
        </tr>
        <tr>
            <td>
                <input type="text" name="name" placeholder="Название">
            </td>
            <td>
                <div class="input-prepend">
                    <a href="#" title="Проиграть"><span class="add-on"><i class="icon-music"></i></span></a>
                    <input id="robotPhotoSong" type="text">
                    <input type="file" style="display: none;" name="sound" id="robotPhotoSongFile">
                </div>
            </td>
            <td>
                <input type="hidden" value="add" name="action" />
                <input type="hidden" value="" name="value" />
                <button class="btn btn-primary">Добавить</button>
            </td>
        </tr>
    </table>
</form>