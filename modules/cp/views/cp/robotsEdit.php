<form class="form-horizontal" method="POST">
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="control-group">
                <div class="span12" style="text-align: right;">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                        <button type="submit" class="btn btn-primary">Применить</button>
                        <button type="submit" class="btn btn-primary">Сохранить и применить</button>
                        <button type="submit" class="btn btn-primary">Сохранить как шаблон</button>
                </div>
            </div>
        </div>
        <div class="span12" style="text-align: center;">
            <h2><?= __('cp_robot_edit_title'); ?> №<?= $robot->id; ?></h2>
        </div>
        <hr />
        <div class="span12" style="text-align: center;">
            <h3>Местоположение</h3>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="robotCity"><?= __('cp_robot_add_city'); ?></label>
                    <div class="controls">
                        <input type="text" id="robotCity" name="city" value="<?= htmlspecialchars($robot->city); ?>" placeholder="<?= __('cp_robot_add_city'); ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="robotAddress"><?= __('cp_robot_add_address'); ?></label>
                    <div class="controls">
                        <input type="text" id="robotCity" name="address" value="<?= htmlspecialchars($robot->address); ?>" placeholder="<?= __('cp_robot_add_address'); ?>">
                    </div>
                </div>

            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="robotPlace"><?= __('cp_robot_add_place'); ?></label>
                    <div class="controls">
                        <input type="text" id="robotCity" name="place" value="<?= htmlspecialchars($robot->place); ?>" placeholder="<?= __('cp_robot_add_place'); ?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="robotInit"><?= __('cp_robot_init'); ?></label>
                    <div class="controls">
                        <input type="checkbox" name="init" id="robotInit"> 
                    </div>
                </div>
            </div>            
        </div>
        <hr />
        <div class="span12" style="text-align: center;">
            <h3>Светодиоды</h3>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">Текущий цвет Фото</label>
                    <div class="controls">
                        <div id="photoColorCur" class="cpColorBox" style="cursor: default; background-color: rgb(255,255,255);">&nbsp;</div>
                        <input type="hidden" name="photoColor" value="255,255,255"/>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">Текущий цвет видео</label>
                    <div class="controls">
                        <div id="videoColorCur" class="cpColorBox" style="cursor: default; background-color: rgb(255,255,255);">&nbsp;</div>
                        <input type="hidden" name="videoColor" value="255,255,255"/>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">Выберите цвет Фото</label>
                    <div class="controls">
                        <? foreach($templates['color'] as $color){ ?>
                        <div class="cpColorBox template" style="background-color: rgb(<?=$color->value;?>);" data-value="<?=$color->value;?>" data-type="photo">&nbsp;</div>
                        <? } ?>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">Выберите цвет видео</label>
                    <div class="controls">
                        <? foreach($templates['color'] as $color){ ?>
                            <div class="cpColorBox template" style="background-color: rgb(<?=$color->value;?>);" data-value="<?=$color->value;?>" data-type="video">&nbsp;</div>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="span12" style="text-align: center;">
            <h3>Звуки</h3>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">Звук фото</label>
                    <div class="controls">
                        <? foreach ($templates['sound'] as $sound) { ?>
                            <audio style="float: left; margin-right: 10px;" src="/upload/sound/<?=$sound->value;?>" controls>KLOL</audio>
                            <div style="padding-top: 5px;"><input type="radio" name="photoSound" style="margin-top: -4px; margin-right: 5px;"><?=$sound->name;?></input></div>
                        <? } ?>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="robotVideoSong">Звук видео</label>
                    <div class="controls">
                        <? foreach ($templates['sound'] as $sound) { ?>
                            <audio style="float: left; margin-right: 10px;" src="/upload/sound/<?=$sound->value;?>" controls>KLOL</audio>
                            <div style="padding-top: 5px;"><input type="radio" name="videoSound" style="margin-top: -4px; margin-right: 5px;"><?=$sound->name;?></input></div>
                        <? } ?>
                    </div>
                </div>
            </div>            
        </div>
        <hr />
        <div class="span12" style="text-align: center;">
            <h3>Задержка</h3>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="robotPhotoTime">Задержка фото</label>
                    <div class="controls">
                        <input type="number" id="robotPhotoTime" name="city" placeholder="1-5" min="1" max="5">
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label class="control-label" for="robotVideoTime">Задержка видео</label>
                    <div class="controls">
                        <input type="number" id="robotVideoTime" name="city" placeholder="1-5" min="1" max="5">
                    </div>
                </div>
            </div>            
        </div>
        <hr />
        <div class="span12" style="text-align: center;">
            <h3>Настройки фотоаппарата</h3>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <label class="control-label">ISO</label>
                    <div class="controls">
                        <select name="photoIso">
                            <? foreach($templates['iso'] as $iso => $desc) { ?>
                                <option value="<?=$iso;?>"><?=$iso;?> - <?=$desc;?></option>
                            <? } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="span6">
            </div>            
        </div>
        <hr />
        <div class="span12" style="text-align: center;">
            <h3>Остальное</h3>
        </div>
        <div class="row-fluid">
            <div class="control-group">
                <div class="span12">
                    <label class="control-label" for="robotAperture"><h4>Примечание</h4></label>
                    <div class="controls">
                        <textarea rows="5"></textarea> 
                    </div>   
                </div>
            </div>
        </div>
    </div>


</form>
