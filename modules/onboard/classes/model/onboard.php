<?php
class Model_Onboard extends ORM {

        /*
         * предопределения
         */
        protected $_primary_key = 'id';
        protected $_table_name = 'onboard';
        
        public $onBoardTypes = array(
            'add_photo' => 1,
            'add_video' => 2,
            'get_photo' => 3,
            'get_video' => 4,
        );
        
        public function add($type, $user = ''){
            if(empty($this->onBoardTypes[$type])){
                return false;
            }
            if(empty($user)){
                if(!Auth::instance()->logged_in()){
                    return false;
                }
                $user = Auth::instance()->get_user()->id;
            }
            
            $row = $this->create();
            $row->action = $this->onBoardTypes[$type];
            $row->user = $user;
            $row->time = time();
            $row->save();
        }
        
        public function getOnboard($limit = 9){
            $data = $this->limit($limit)->order_by('id', 'desc')->find_all();
            
            $result = array();
            foreach($data as $row){
                $user = ORM::factory('user', $row->user);
                $url = empty($row->url) ? '#' : $row->url;
                $result[] = array(
                    'user'      => array('id' => $user->id, 'avatar' => $user->avatar, 'login' => $user->username),
                    'action'    => $row->action,
                    'time'      => $row->time,
                    'id'        => $row->id,
                    'url'       => $url
                );
            }
            
            return $result;
        }
}
?>