<?php defined('SYSPATH') or die('No direct script access.');

return array(
    
    'VK_APP_ID'             => '3580751', // ID приложения
    
    'VK_APP_SECRET'         => 'nq3WyB5P2ku77We7zEMF', // Защищенный ключ
    
    'REDIRECT_URI'          => 'http://mpr.frostmoon.name/user/loginvk/',
    
    'DISPLAY'               => 'page', // page OR popup OR touch OR wap
    
    'SCOPE'                 => array(
            'notify', // Пользователь разрешил отправлять ему уведомления.
            'friends', // Доступ к друзьям. 
            'photos', // Доступ к фотографиям. 
            'audio', // Доступ к аудиозаписям. 
            'video', // Доступ к видеозаписям.
            'docs', // Доступ к документам.
            'notes', // Доступ заметкам пользователя. 
            'pages', // Доступ к wiki-страницам. 
            'wall', // Доступ к обычным и расширенным методам работы со стеной. 
            'groups', // Доступ к группам пользователя. 
            'ads', // Доступ к расширенным методам работы с рекламным API. 
            'offline' // Доступ к API в любое время со стороннего сервера. 
    ), 
    
    'VK_URI_METHOD'         => 'https://api.vk.com/method/{METHOD_NAME}?{PARAMETRS}&access_token={ACCESS_TOKEN}',
    'VK_URI_AUTH'           => 'http://api.vk.com/oauth/authorize?client_id={CLIENT_ID}&redirect_uri={REDIRECT_URI}&scope={SCOPE}&display={DISPLAY}',
    'VK_URI_ACCESS_TOKEN'   => 'https://oauth.vk.com/access_token?client_id={CLIENT_ID}&client_secret={APP_SECRET}&code={CODE}&redirect_uri={REDIRECT_URI}&',
    'VK_URI_METHOD'         => 'https://api.vk.com/method/{METHOD_NAME}?{PARAMETERS}&access_token={ACCESS_TOKEN}',
);
