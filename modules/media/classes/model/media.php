<?php
class Model_Media extends ORM
{

    /*
     * предопределения
     */
    protected $_primary_key = 'id';
    protected $_table_name = 'media';
    public $curLoadFile = '';
    public $curLoadRobot = '';

    /**
     * Загружает фото и превьюшку
     * @param $files  массив $_FILES
     * @return array  Массив с флагом ошибки и сообщением
     */
    public function loadPhoto($files)
    {
        $result = array('error' => false, 'msg' => '');
        // Основную фотку
        if (!empty($files['full'])) {
            $this->curLoadFile = md5($files['full']['name'] . time()).".jpg";
            $result = $this->loadPhotoFile($files['full'], $_SERVER['DOCUMENT_ROOT'] . "/upload/photos/f/");
        } else {
            $result = array('error' => true, 'msg' => '[\'ERROR\']: Empty full file');
        }
        if($result['error']){
            return $result;
        }

        // Превьюшку фотку
        if (!empty($files['thumb'])) {
            $result = $this->loadPhotoFile($files['thumb'], $_SERVER['DOCUMENT_ROOT'] . "/upload/photos/t/");
        } else {
            $result = array('error' => true, 'msg' => '[\'ERROR\']: Empty thumb file');
        }
        if($result['error']){
            return $result;
        }
        
        $owner = rand(4, 7);
        
        
        // Добавим OnBoard
        $model_onboard = new Model_Onboard();
        $model_onboard->add('add_photo', $owner);
        
        // Добавим количество
        $model_robot = new Model_Robot();
        $model_robot->addPhotoCount($this->curLoadRobot);

        // Сохраним в базу
        $t = $this->create();
        $t->owner = $owner;
        $t->robot = 1;
        $t->type = 0;
        $t->name = $this->curLoadFile;
        $t->save();

        return $result;
    }

    /**
     * Загружает видео и превьюшки
     * @param $files  массив $_FILES
     * @return array  Массив с флагом ошибки и сообщением
     */
    public function loadVideo($files)
    {
        $result = array('error' => false, 'msg' => '');
        // Видео
        if (!empty($files['full'])) {
            $this->curLoadFile = md5($files['full']['name'] . time()).".flv";
            $result1 = $this->loadVideoFile($files['full'], $_SERVER['DOCUMENT_ROOT'] . "/upload/videos/f/");
            $this->curLoadFile = str_replace("flv", "webm", $this->curLoadFile);
            $result2 = $this->loadVideoFile($files['fullweb'], $_SERVER['DOCUMENT_ROOT'] . "/upload/videos/f/");
            $result = array_merge($result1, $result2);
        } else {
            $result = array('error' => true, 'msg' => '[\'ERROR\']: Empty full file');
        }
        if($result['error']){
            return $result;
        }

        $originalFile = $this->curLoadFile;
        // Превьюшки
        for($t = 1; $t <= 3; $t++){
            if (!empty($files['thumb'.$t])) {
                $result = $this->loadPhotoFile($files['thumb'.$t], $_SERVER['DOCUMENT_ROOT'] . "/upload/videos/t/", $t);
            } else {
                $result = array('error' => true, 'msg' => '[\'ERROR\']: Empty thumb file');
            }
            if($result['error']){
                return $result;
            }
        }
        $this->curLoadFile = $originalFile;

        $owner = rand(4, 7);


        // Добавим количество
        $model_robot = new Model_Robot();
        $model_robot->addPhotoCount($this->curLoadRobot);
        
        // Добавим OnBoard
        $model_onboard = new Model_Onboard();
        $model_onboard->add('add_video', $owner);

        // Сохраним в базу
        $t = $this->create();
        $t->owner = $owner;
        $t->robot = 1;
        $t->type = 1;
        $t->name = $this->curLoadFile;
        $t->save();

        return $result;
    }

    /**
     * Метод проверяющий картинку и перемещающий куда надо
     * @param $file массив из массива $_FILES
     * @param $path Путь куда сохранят картинку (photo thumb, photo full, video thumb)
     * @return array Массив с флагом ошибки и сообщением
     */
    public function loadPhotoFile($file, $path, $videoThumb = false)
    {
        $type = 'image/jpeg';
        $result = array('error' => false, 'msg' => '');
        if ($file['type'] != $type) {
            $result['msg'] .= ' ,[OK]: tmp in ' . $file['tmp_name'];
            if(strpos($this->curLoadFile, '.flv')){
                $this->curLoadFile = str_replace(".flv", "_" .$videoThumb. ".jpg", $this->curLoadFile);
            }
            $newFileName = $path.$this->curLoadFile;
            if (copy($file['tmp_name'], $newFileName)) {
                return array('error' => false, 'msg' => $result['msg'] . ' ,[OK]: stored in ' . $newFileName);
            } else {
                return array('error' => true, 'msg' => $result['msg'] . '[\'ERROR\']: Can\'t copy file '.$file['name']);
            }
        } else {
            return array('error' => true, 'msg' => '[\'ERROR\']: Bad mime-type full photo'.$file['name']);
        }
    }

    /**
     * Метод проверяющий видео и перемещающий куда надо
     * @param $file массив из массива $_FILES
     * @param $path Путь куда сохранят видео
     * @return array Массив с флагом ошибки и сообщением
     */
    public function loadVideoFile($file, $path)
    {
        //$type = 'application\octet-stream';
        $result = array('error' => false, 'msg' => '');
        //if ($file['type'] != $type) {
            $result['msg'] .= ' ,[OK]: tmp in ' . $file['tmp_name'];
            $newFileName = $path.$this->curLoadFile;
            if (copy($file['tmp_name'], $newFileName)) {
                return array('error' => false, 'msg' => $result['msg'] . ' ,[OK]: stored in ' . $newFileName);
            } else {
                return array('error' => true, 'msg' => $result['msg'] . '[\'ERROR\']: Can\'t copy file '.$file['name']);
            }
        //} else {
            //return array('error' => true, 'msg' => '[\'ERROR\']: Bad mime-type full video ' . $file['type'] . '|'. $type .' -' .$file['name']);
        //}
    }

    /**
     * Получает все медиа файлы (без учета каких либо фильтров)
     *
     * @return array
     */
    public function getAllMedia()
    {
        $res = parent::find_all();

        $media = array();
        foreach ($res as $row) {
            $folder = $row->type == 0 ? 'photos' : 'videos';

            $media[$row->id] = array(
                'src' => array(
                    'full' => '/upload/' . $folder . '/f/' . $row->name,
                    'thumb' => '/upload/' . $folder . '/t/' . $row->name,
                )
            );
        }

        return $media;
    }

    /**
     * Получает все медиа файлы для модерации
     *
     * @return array
     */
    public function getAllMediaForModerate()
    {
        $res = parent::where('moderate', '=', '0')->find_all();

        $media = array();
        foreach ($res as $row) {
            $folder = $row->type == 0 ? 'photos' : 'videos';

            $media[$row->id] = array(
                'src' => array(
                    'full' => '/upload/' . $folder . '/f/' . $row->name,
                    'thumb' => '/upload/' . $folder . '/t/' . $row->name,
                )
            );
        }

        return $media;
    }

    /**
     * Получение данных по медиа файлам
     *
     * @param array $params массив с условиями поиска
     * @return array массив с элементами медиа контента
     */
    public function getMedia($params = array())
    {
        $exceptions = array('page', 'per_page', 'with_private', 'city', 'place', 'cur_position');

        // получим id пользователя по нику
        if (!is_null($params['owner'])) {
            $owner = ORM::factory('user')->where('username', '=', $params['owner'])->find();
            $params['owner'] = $owner->id;
        }

        // получим id робота по адресу\месту
        if (!is_null($params['city']) || !is_null($params['place'])) {
            $robot = ORM::factory('robot');
            if (!is_null($params['city'])) {
                $robot->where('city', '=', $params['city']);
            }
            if (!is_null($params['place'])) {
                $robot->where('place', '=', $params['place']);
            }
            $robot->find();
            $params['robot'] = $robot->id;
        }

        $selector = $this->select(array('id', 'id'), array('name', 'name'));
        foreach ($params as $k => $v) {
            if (!in_array($k, $exceptions) && !is_null($v)) {
                $selector->where($k, '=', $v);
            }
        }
        $selector->where('private', '=', 0);
        $size = $selector->count_all(false);
        if($params['cur_position'] > 0){
            $selector->offset($params['cur_position']);    
        }
        $obResult = $selector->limit($params['per_page'])->order_by('id', 'desc')->find_all();
        
        //var_dump($obResult);
        
        $result = array();
        foreach ($obResult as $key => $value) {
            $_owner = ORM::factory('user')->where('id', '=', $value->owner)->find();
            $_robot = ORM::factory('robot')->where('id', '=', $value->robot)->find();

            $result['result'][] = array('id' => $value->id, 'src' => $value->name, 'type' => $value->type,
                'desc' => htmlspecialchars($_owner->username) ."|". htmlspecialchars($_robot->city) ."|". htmlspecialchars($_robot->place)
            );
        }

        $result['pages'] = (int)ceil((int)$size / (int)$params['per_page']);
        return $result;
    }

    /**
     * Получения количества медиафайлов
     *
     * @param bool withPrivate - учитывать ли приватные медиа-элементы
     * @return array массив с двумя значениями (фото и видео)
     */
    public function getMediaCount($withPrivate = true)
    {
        if ($withPrivate) {
            $obResult = $this->select(array('type', 'type'), array('COUNT("*")', 'count'))
                ->group_by('type')
                ->find_all()
                ->as_array('type', 'count');
        } else {
            $obResult = $this->select(array('type', 'type'), array('COUNT("*")', 'count'))
                ->where('private', '=', 0)
                ->where('owner', '=', Auth::instance()->get_user()->id)
                ->group_by('type')
                ->find_all()
                ->as_array('type', 'count');
        }

        if (!isset($obResult[0])) {
            $obResult[0] = 0;
        }
        if (!isset($obResult[1])) {
            $obResult[1] = 0;
        }

        return $obResult;
    }

    /**
     * Получение критериев для поиска (город, место)
     *
     * @return array массив с двумя списками (города и места)
     */
    public function getMediaCriteria()
    {
        $robot = ORM::factory('robot');
        $obResult = $robot->select(array('city', 'city'), array('place', 'place'))->find_all();

        $result = array('city' => array(), 'place' => array());
        foreach ($obResult as $robot) {
            $result['city'][] = $robot->city;
            $result['place'][] = $robot->place;
        }

        $result['city'] = array_unique($result['city']);
        $result['place'] = array_unique($result['place']);

        return $result;
    }

    /**
     * Получение медиафайла для просмотра
     *
     * @param $id - id файла
     * @param bool withPrivate - учитывать ли приватные медиа-элементы
     * @return array массив с данными по файлу
     */
    public function getMediaById($id, $withPrivate = true)
    {
        $obResult = parent::where('id', '=', $id);
        if ($withPrivate) {
            $obResult->where('owner', '=', Auth::instance()->get_user()->id);
        }
        
        $obResult = $obResult->find_all();
        
        $size =  parent::where('id', '>', $id);
        if ($withPrivate) {
            $size->where('owner', '=', Auth::instance()->get_user()->id);
        }
        $page = $size->count_all(false);
        $page = (int)ceil((int)$page / 20);
        
        $result = array();
        foreach ($obResult as $key => $value) {
            $result = array('id' => $value->id, 'owner' => $value->owner, 'src' => $value->name, 'time' => $value->time, 'type' => $value->type, 'page' => $page, 'robot' => $value->robot);
        }

        $modelRobot = new Model_Robot();
        $result['robot'] = $modelRobot->getRobotPlace($result['robot']);

        $user = ORM::factory('user', $result['owner']);
        $result['owner'] = array('id' => $user->id, 'username' => $user->username);


        return $result;
    }

} // End Currency Model
?>
