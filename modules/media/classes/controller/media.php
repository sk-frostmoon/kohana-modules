<?php

class Controller_Media extends Controller_Common {

    /**
     * Возвращает Блок для результатов медиа данных для профайла
     */
    public function action_ajaxSearchCriteria() {
        if ($this->request->is_ajax()) {
            $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array()));
        }
    }

    /**
     * Возвращает Блок для результатов медиа данных для профайла
     */
    public function action_profileResultBlock() {
        $this->auto_render = false;
        $content = View::factory('media/profileResultBlock');
        $this->response->body($content);
    }

    /**
     * Возвращает Блок для результатов медиа данных для главной
     */
    public function action_mainResultBlock() {
        $this->auto_render = false;
        $content = View::factory('media/mainResultBlock');
        $this->response->body($content);
    }

    /**
     * Возвращает объект с результатами с медиа данными для главной
     */
    public function action_mainResult() {
        $this->action_profileResult(true);
    }

    /**
     * Возвращает объект с результатами с медиа данными для профайла
     */
    public function action_profileResult($main = false) {
        //if($this->request->is_ajax()){
        $this->auto_render = false;

        /*
         * Получение параметров для поиска 
         */
        $params = array(
            //'page' => $this->request->query('page'),
            'per_page' =>  $this->request->query('per_page'), // фикс
            'cur_position' => $this->request->query('cur_position'),
            'city' => $this->request->query('city'),
            'place' => $this->request->query('place'),
            'owner' => $this->request->query('user'),
            'with_private' => true
        );

        if (!$main && Auth::instance()->logged_in()) {
            $params['owner'] = Auth::instance()->get_user()->username;
            $params['with_private'] = false;
        }

        $model = new Model_Media();
        $media = $model->getMedia($params);

        $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode($media));
        // }
    }

    public function action_view() {
        $id = $this->request->param('id');
        if ((int) $id > 0) {
            $model = new Model_Media();
            $mediaEl = $model->getMediaById($id, false);
            if (count($mediaEl) > 0) {
                if ($this->request->is_ajax()) {
                    $this->auto_render = false;
                    $this->template->set_filename('main/main_empty');
                    $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array_merge(array('status' => '0'), $mediaEl)));
                } else {
                    if(strtotime($mediaEl['time']) <= 0){
                        $mediaEl['time'] = "";
                    } else {
                        $mediaEl['time'] = date("d.m.Y H:i");
                    }
                    $this->auto_render = true;
                    $content = View::factory('media/view')
                            ->set('mediaEl', $mediaEl);

                    $this->template->content = $content;
                }
            } else {
                $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array('status' => '0', 'message' => 'Не найдено')));
            }
        } else {
            $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array('status' => '0', 'message' => 'Не передан параметр')));
        }
    }

    /**
     * Получение данных по указанному дню
     *
     * @return void
     */
    public function action_load() {
        $this->auto_render = false;
        $model_robot = new Model_Robot();
        $answer = array(
            'error' => '0',
            'ping' => '0',
            'xml' => '0',
            'xml_data' => '',
            'load' => '0',
            'load_data' => ''
        );

        $data = Arr::extract($_POST, array('robot_name', 'ping', 'xml', 'load'));

        $robotId = $data['robot_name'];
        if((int)$robotId > 0){
            // Пингуем
            if($data['ping'] == '1'){
                $model_robot->updateStatus($robotId);
                $answer['ping'] = '1';
            }

            // Отдаем XML конфиг
            if($data['xml'] == '1'){
                //$model_robot->getConfig($robotId);
                //$answer['xml'] = '1';
                //$answer['xml_data'] = '';
            }

            // Загружаем файлы
            if($data['load'] == '1'){
                $answer['load'] = '1';

                $fo = fopen($_SERVER['DOCUMENT_ROOT'] . "/load_log.txt", "a+");
                $log = '[' . date("H:i:s") . ']';
                if(!empty($_FILES)){
                    $type = Arr::extract($_POST, array('type'));
                    $_loadResult = array('error' => false, 'msg' => '');
                    $mediaModel = new Model_Media();
                    $mediaModel->curLoadRobot = $robotId;
                    switch($type['type']){
                        case "photo":
                            $_loadResult = $mediaModel->loadPhoto($_FILES);
                            break;
                        case "video":
                            $_loadResult = $mediaModel->loadVideo($_FILES);
                            break;
                        default:
                            $log .= ' ,[ERROR]: unknown type';
                            $answer['error'] = '1';
                            $answer['load_data'] = $log;
                            break;
                    }
                    $log .= $_loadResult['msg'];
                    if($_loadResult['error']){
                        $answer['error'] = '1';
                        $answer['load_data'] = $log;
                    }
                } else {
                    $log .= ' ,[ERROR]: empty file';
                    $answer['error'] = '1';
                    $answer['load_data'] = $log;
                }
                fwrite($fo, $log . "\r\n");
                fclose($fo);
            }
        } else {
            $answer['error'] = '1';
        }
        
        $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode($answer));
    }
    
    public function action_redactor(){
        $this->auto_render = true;
        if(!Auth::instance()->logged_in()){
            Request::current()->redirect('/');
        }

        $id = $this->request->param('id');
        $model = new Model_Media();
        
        $data = Request::factory('/media/profileResult/?page=1&per_page=10')->execute();
        $mediaList = json_decode($data->body(), true);
        if(is_array($mediaList)){
            $mediaList = $mediaList['result'];
        }

        if ((int) $id > 0) {
            if(!empty($mediaList[$id])){
                $mediaEl = $mediaList[$id];
                $mediaEl['id'] = $id;
            } else {
                $mediaEl = $model->getMediaById($id, true);
            }
        } else {
            $mediaEl = current($mediaList);
        }
        
//        if ((int) $id > 0) {
//            $model = new Model_Media();
//            $mediaEl = $model->getMediaById($id, false);
//            if (count($mediaEl) > 0) {
//                if ($this->request->is_ajax()) {
//                    $this->auto_render = false;
//                    $this->template->set_filename('main/main_empty');
//                    $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array_merge(array('status' => '0'), $mediaEl)));
//                } else {
//                    $this->auto_render = true;
//                    $content = View::factory('media/view')
//                            ->set('mediaEl', $mediaEl);
//
//                    $this->template->content = $content;
//                }
//            } else {
//                $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array('status' => '0', 'message' => 'Не найдено')));
//            }
//        } else {
//            $this->response->headers('Content-Type', 'application/json; charset=utf-8')->body(json_encode(array('status' => '0', 'message' => 'Не передан параметр')));
//        }
//        
        $content = View::factory('media/redactor')
                ->set('auth', Auth::instance())
                ->set('mediaEl', $mediaEl)
                ->set('mediaList', $mediaList)
                ;
        $this->template->title .= ' | Редактор - ' . Auth::instance()->get_user()->username;
        
        $this->template->content = $content;
    }
}

// End Currency Controller
?>
