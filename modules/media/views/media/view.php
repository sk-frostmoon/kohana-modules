<section class="all_page">
    <? /* Start of OnBoard */ ?>
    <?= Request::factory('user/onBoard')->execute(); ?>
    <? /* End of OnBoard */ ?>

    <? /* Start of Main Searchform */ ?>
    <div class="all_page_bord for_sky">
        <?= Request::factory('search/mainForm')->execute(); ?>
    </div>
    <? /* End of Main Searchform */ ?>

    <div class="all_page_bord" id="view">
        <div class="wrappernavi">
            <div class="photo_slider">
                <ul>
                    <li>
                        <div class="photo_big_image mainPlayer" data-page="<?=$mediaEl['page'];?>" data-id="<?=$mediaEl['id'];?>">
                            <? if($mediaEl['type'] == '0'){ ?>
                                <img class="fullview" src="http://mpr.frostmoon.name/upload/photos/f/<?=$mediaEl['src'];?>" alt="#" width="700" data-id="<?=$mediaEl['id'];?>"/>
                            <? } else { ?>
                                <video class="fullview" width="700" controls autoplay>
                                        <source src="http://mpr.frostmoon.name/upload/videos/f/<?=str_replace("flv", "webm", $mediaEl['src']);?>" type="video/webm">
                                        <source src="http://mpr.frostmoon.name/upload/videos/f/<?=str_replace("webm", "mp4", $mediaEl['src']);?>" type="video/mp4">
                                        <object width="700" type="application/x-shockwave-flash" data="/public/player.swf">
                                                <param name="movie" value="/public/player.swf" />
                                                <param name="flashvars" value="autostart=true&amp;controlbar=over&amp;file=<?=$mediaEl['src'];?>" />
                                                <img src="" width="700" alt="11111"
                                                     title="No video playback capabilities, please download the video below" />
                                        </object>
                                </video>
                            <? } ?>
                            <br /><br /><br />
                            <a class="play_prev" href="#view"></a>
                            <a class="play_next" href="#view"></a>
                            <div class="photo_slider_control">                                                                                
                                <div class="slider_share">
                                    <a href="#">Отметиться</a>
                                    <a href="#">Забрать к себе</a>
                                </div>
                                <div class="slider_place">
                                    <span><?=$mediaEl['robot']['city'];?>, <?=$mediaEl['robot']['place'];?><?=!empty($mediaEl['time'])?", ".$mediaEl['time']:"";?></span>
                                </div>
                                <div class="slider_social">
                                    <table>
                                        <tr>
                                            <td>
                                                <a href="#" class="slide_fb"></a>    
                                            </td>
                                            <td>
                                                <a href="#" class="slide_vk"></a>    
                                            </td>
                                        </tr>
                                    </table>                         
                                </div>   
                            </div>
                            <div class="who_on_photo">
                                <h5>На фото</h5>
                                <p>
                                    <a class="red" href="#<?=$mediaEl['owner']['id'];?>"><?=$mediaEl['owner']['username'];?></a>
                                </p>
                                <a href="" onclick="return false;" class="hide"></a>
                            </div>
                        </div>                               
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?= Request::factory('media/mainResultBlock')->execute(); ?>    
    <?
    $model = new Model_Media();
    $media = $model->getMediaCriteria();
    ?>

    <div class="all_page_bord">
        <div class="throughline">
            <div class="wrapper">
                <div class="banner">
                    <img src="/public/images/banner.png" alt="#" />
                </div>
            </div>
        </div>
    </div>
    <div class="all_page_bord">
        <div class="wrapper">
            <div class="row spon">
                <div class="span2">
                    <img src="/public/images/spons1.png" alt="#" />    
                </div>
                <div class="span2">
                    <img src="/public/images/spons2.png" alt="#" />    
                </div>
                <div class="span2">
                    <img src="/public/images/spons3.png" alt="#" />    
                </div>
                <div class="span2">
                    <img src="/public/images/spons4.png" alt="#" />    
                </div>
                <div class="span2">
                    <img src="/public/images/spons5.png" alt="#" />    
                </div>
                <div class="span2">
                    <img src="/public/images/spons6.png" alt="#" />    
                </div>
            </div>
        </div>
    </div>
</section>