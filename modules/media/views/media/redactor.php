<? $socials = unserialize($auth->get_user()->socials); ?>
<section class="all_page">
    <div class="all_page_bord for_sky">
        <div class="wrapper">
            <div id="sidebar">              
                <div class="ava_block">
                    <h4><a href="#"><?= $auth->get_user()->username; ?></a></h4>
                    <?
                    $myAvatar = "";
                    strlen($auth->get_user()->avatar) > 0 ? $myAvatar = "/upload/avatars/f/" . $auth->get_user()->avatar : $myAvatar = "/public/images/def_ava_big.png";
                    ?>
                    <img src="<?= $myAvatar; ?>" alt="#" width="147" height="125" />
                    <p class="status"><span>Профессионал</span></p>
                </div>
                <div class="social_links">
                    <h6><?= __('profile_about'); ?></h6>
                    <ul>
                        <li class="fb">
                            <a <?= $socials['soc_fb'] ? 'href="' . $socials['soc_fb'] . '" target="_blank"' : 'href="#"'; ?>><?= $auth->get_user()->username; ?></a>    
                        </li>
                        <li class="vk">
                            <a  <?= $socials['soc_vk'] ? 'href="' . $socials['soc_vk'] . '" target="_blank"' : 'href="#"'; ?>><?= $auth->get_user()->username; ?></a>    
                        </li>
                        <li class="tw">
                            <a  <?= $socials['soc_tw'] ? 'href="' . $socials['soc_tw'] . '" target="_blank"' : 'href="#"'; ?>><?= $auth->get_user()->username; ?></a>    
                        </li>
                    </ul>
                </div>
                <div class="filter">
                    <div class="filter_bg">
                        <form action="#" method="post">
                            <div class="date_post">
                                <select class="sel">
                                    <option></option>
                                    <option></option>
                                    <option></option>
                                </select>
                            </div>
                            <div class="name_search">
                                <input type="text" value="поиск по названию"/>
                            </div>
                            <div class="city_search">
                                <select class="sel" value="поиск по городу">
                                    <option>поиск по городу</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                </select>
                            </div>
                            <div class="country_search">
                                <select class="sel" value="поиск по стране">
                                    <option>поиск по стране</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                </select>
                            </div>
                            <div class="studio_search">
                                <select class="sel" value="поиск по студии">
                                    <option>поиск по студии</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                    <option>название города</option>
                                </select>
                            </div>
                            <div class="search_but_filter">
                                <input type="submit" value="Искать"/>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="redactor">
                    <a href="/media/redactor/">Фоторедактор</a>
                </div>
            </div>
            <div id="rightcol">
                <div class="page_redactor">
                    <h4>Фоторедактор</h4>
                    <a class="close_redactor" href="/user/profile/">Вернуться</a>
                </div>
                <div class="redact_photo">
                    <img src="http://mpr.frostmoon.name/upload/photos/f/<?= $mediaEl['src']; ?>" alt="#" title="#" width="740" height="487">
                    <div class="edit">
                        <a class="top_edit" href="#"></a>
                        <a class="bottom_edit" href="#"></a>
                        <a class="left_edit" href="#"></a>
                        <a class="right_edit" href="#"></a>
                        <a class="left_top_treu" href="#"></a>
                        <a class="right_bottom_treu" href="#"></a>
                    </div>
                </div>                          
                <div class="photonavi">
                    <a class="prev" href="#"> </a>
                    <div class="naviphotos">
                        <?
                        foreach ($mediaList as $mediaListKey => $mediaListEl) {
                            ?><a href="/media/redactor/<?= $mediaListKey; ?>/"><img src="http://mpr.frostmoon.name/upload/photos/t/<?= $mediaListEl['src']; ?>" alt="#"></a><? }
                        ?>
                    </div>                 
                    <a class="next" href="#"> </a>
                </div>                                                
            </div>
        </div>
        <div class="all_page_bord">
            <div class="wrapper"> 
                <div class="tochange">
                    <div class="change_but">
                        <a href="#" class="cadr">Кадрировать</a>
                        <a href="#" class="prim">Применить</a>
                    </div>
                    <div class="slider_social all_social">
                        <table>
                            <tbody><tr>
                                    <td>
                                        <a href="#" class="slide_tw"></a>    
                                    </td>
                                    <td>
                                        <a href="#" class="slide_fb"></a>    
                                    </td>
                                    <td>
                                        <a href="#" class="slide_gh"></a>    
                                    </td>
                                    <td>
                                        <a href="#" class="slide_vk"></a>    
                                    </td>
                                    <td>
                                        <a class="all_serv" href="#">все сервисы</a>    
                                    </td>
                                </tr>
                            </tbody></table>                         
                    </div>
                </div>
            </div>
        </div>
        <div class="all_page_bord">
            <div class="wrapper">
                <a class="close_op close_filter" href="#"></a>
                <div class="many_filt">
                    <table>
                        <tbody><tr>
                                <td>
                                    Фильтр 1    
                                </td>
                                <td>
                                    <img src="/public/images/filter1.png" alt="#">    
                                </td>
                                <td>                                    
                                    <a href="#" class="value_left"></a>
                                    <div id="slider-range-min" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>
                                    <a href="#" class="value_right"></a>
                                    <div class="boxer">
                                        <input type="text" id="amount" style="border: 0; color: #666; width: 22px; text-align: center;">
                                        <span class="percent">%</span>
                                    </div>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Фильтр 2    
                                </td>
                                <td>
                                    <img src="/public/images/filter1.png" alt="#">    
                                </td>
                                <td>
                                    <a href="#" class="value_left"></a>
                                    <div id="slider-range-min2" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>
                                    <a href="#" class="value_right"></a>
                                    <div class="boxer">
                                        <input type="text" id="amount2" style="border: 0; color: #666; width: 22px; text-align: center;">
                                        <span class="percent">%</span>
                                    </div>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Фильтр 3    
                                </td>
                                <td>
                                    <img src="/public/images/filter1.png" alt="#">    
                                </td>
                                <td>
                                    <a href="#" class="value_left"></a>
                                    <div id="slider-range-min3" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>
                                    <a href="#" class="value_right"></a>
                                    <div class="boxer">
                                        <input type="text" id="amount3" style="border: 0; color: #666; width: 22px; text-align: center;">
                                        <span class="percent">%</span>
                                    </div>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Фильтр 4    
                                </td>
                                <td>
                                    <img src="/public/images/filter1.png" alt="#">    
                                </td>
                                <td>
                                    <a href="#" class="value_left"></a>
                                    <div id="slider-range-min4" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>
                                    <a href="#" class="value_right"></a>
                                    <div class="boxer">
                                        <input type="text" id="amount4" style="border: 0; color: #666; width: 22px; text-align: center;">
                                        <span class="percent">%</span>
                                    </div>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Фильтр 5    
                                </td>
                                <td>
                                    <img src="/public/images/filter1.png" alt="#">    
                                </td>
                                <td>
                                    <a href="#" class="value_left"></a>
                                    <div id="slider-range-min5" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" aria-disabled="false"><div class="ui-slider-range ui-widget-header ui-slider-range-min" style="width: 0%;"></div><a class="ui-slider-handle ui-state-default ui-corner-all" href="#" style="left: 0%;"></a></div>
                                    <a href="#" class="value_right"></a>
                                    <div class="boxer">
                                        <input type="text" id="amount5" style="border: 0; color: #666; width: 22px; text-align: center;">
                                        <span class="percent">%</span>
                                    </div>                                    
                                </td>
                            </tr>
                        </tbody></table>
                </div>
            </div>       
        </div>
        <div class="all_page_bord">
            <div class="throughline">
                <div class="wrapper">
                    <div class="banner">
                        <img src="/public/images/banner.png" alt="#" />
                    </div>
                </div>
            </div>
        </div>           
</section>                                

<? /*
  <?php $socials = unserialize($auth->get_user()->socials); ?>
  <div class="container userProfile_container">
  <div class="col1">
  <p class="username"><?=$auth->get_user()->username;?></p>
  <div class="closer" style="height: 10px;"></div>
  <a href="" id="changeAvatar">
  <img src="/upload/avatars/f/<?=$auth->get_user()->avatar;?>" class="avatar" />
  </a>
  <div class="closer" style="height: 10px;"></div>
  <p>Профессионал</p>
  <div class="closer" style="height: 30px;"></div>

  <p><?=__('profile_about');?></p>
  <div class="closer" style="height: 10px;"></div>
  <div class="socialSmallVk">
  <?=$socials['soc_fb']?'<a href="'.$socials['soc_fb'].'" target="_blank">':'';?>
  <div><?=__('profile_fb');?></div>
  <?=$socials['soc_fb']?'</a>':'';?>
  </div>
  <div class="closer" style="height: 10px;"></div>
  <div class="socialSmallFb">
  <?=$socials['soc_vk']?'<a href="'.$socials['soc_vk'].'" target="_blank">':'';?>
  <div><?=__('profile_vk');?></div>
  <?=$socials['soc_vk']?'</a>':'';?>
  </div>
  <div class="closer" style="height: 10px;"></div>
  <div class="socialSmallTw">
  <?=$socials['soc_tw']?'<a href="'.$socials['soc_tw'].'" target="_blank">':'';?>
  <div><?=__('profile_tw');?></div>
  <?=$socials['soc_tw']?'</a>':'';?>
  </div>
  </div>
  <div class="col2">
  <?php
  if($customize){
  ?>
  <?=Form::open('/user/profile?customize=yes')?>
  <a href="/user/profile/"><div class="customizeButton"><?=__('profile_profile');?></div></a>
  <div class="closer" style="height: 20px; clear: both;"></div>
  <div class="_3">
  <div class="customizeLabel" style="margin-top: 2px;"><?=__('profile_show_page');?></div>
  <div class="_buttonActive" data-id="1"><?=__('profile_show_page_all');?></div>
  <div class="_buttonInactive"  data-id="0" style="margin-left: -13px;"><?=__('profile_show_page_none');?></div>
  <?= Form::hidden('private_page', $auth->get_user()->private_page, array('id' => 'profile_private_page_hidden'))?>
  </div>
  <div class="_2">
  <?=Form::button('save', __('profile_save'), array('class' => 'profileSaveButton'));?>
  <?=Form::button('save', __('profile_delete'), array('class' => 'profileDeleteButton'));?>
  </div>
  <div class="closer" style="height: 20px; clear: both;"></div>
  <div class="_1">
  <div class="customizeLabel"><?=__('profile_edit_me');?></div>
  </div>
  <div class="_4">
  <?= Form::input('soc_vk', $socials['soc_vk'], array('class' => 'textInput', 'placeholder' => 'http://vk.com/username', 'style' => 'width: 230px;'))?>
  <?= Form::input('soc_fb', $socials['soc_fb'], array('class' => 'textInput', 'placeholder' => 'http://facebook.com/username', 'style' => 'width: 230px;'))?>
  <?= Form::input('soc_tw', $socials['soc_tw'], array('class' => 'textInput', 'placeholder' => 'http://twitter.com/username', 'style' => 'width: 230px;'))?>
  </div>
  <div class="closer" style="height: 20px; clear: both;"></div>
  <div class="_2">
  <div class="customizeLabel"><?=__('profile_show_all');?></div>
  <div id="privateMediaShow<?=$auth->get_user()->private_media == 1 ? "Active" : "";?>">&nbsp;</div>
  <?=Form::button('', 'Показать все', array('id' => 'privateMediaShowButton'));?>
  </div>
  <div class="_3">1</div>
  <div class="closer" style="height: 20px; clear: both;"></div>
  <div class="_2">
  <div class="customizeLabel"><?=__('profile_hide_all');?></div>
  <div id="privateMediaHide<?=$auth->get_user()->private_media == 0 ? "Active" : "";?>">&nbsp;</div>
  <?=Form::button('', 'Скрыть все', array('id' => 'privateMediaHideButton'));?>
  </div>
  <?= Form::hidden('private_media', $auth->get_user()->private_media, array('id' => 'profile_private_media_hidden'))?>
  <div class="_3">1</div>
  <?=Form::close()?>
  <?php
  } else {
  ?>
  <a href="/user/profile/?customize=yes" onclick=""><div class="customizeButton"><?=__('profile_customize');?></div></a>
  <?=Request::factory('media/profileResultBlock')->execute();?>
  <?php
  }
  ?>
  </div>
  </div>
  <div style="clear: both;"></div>
  <?=$avatarPopup;?>
 */ ?>